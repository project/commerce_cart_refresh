# Commerce Cart Refresh

This module add some Ajax behaviors to the _Add to Cart_ form in Drupal Commerce 2.x in order to dynamically refresh the Price when the Quantity changes.

## Core features
This module is still experimental. It covers the following features for now:

* **Refresh Price when Quantity changes** in _add_to_cart_ form

## Development
Development is happening in this repository on Gitlab.

Please, use [the drupal.org issue queue](https://www.drupal.org/project/issues/commerce_cart_refresh?categories=All) to create issues.
